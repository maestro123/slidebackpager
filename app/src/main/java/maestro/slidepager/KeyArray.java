package maestro.slidepager;

import java.util.Arrays;

/**
 * Created by maestro on 27.10.2016.
 */

public class KeyArray<K, V> {
    private static final int INCREMENT_CAPACITY = 12;

    private Object[] mKeys = new Object[INCREMENT_CAPACITY];
    private Object[] mValues = new Object[INCREMENT_CAPACITY];

    private Object[] mSortedKeys = new Object[INCREMENT_CAPACITY];
    private Object[] mSortedValues = new Object[INCREMENT_CAPACITY];

    private int mSize = 0;

    private boolean isKeysDirty = true;
    private boolean isValuesDirty = true;

    public int size() {
        return mSize;
    }

    public void put(K key, V value) {
        checkCapacity();

        mKeys[mSize] = key;
        mValues[mSize] = value;
        mSize += 1;
        isKeysDirty = true;
        isValuesDirty = true;
    }

    public V removeByKey(K key) {
        int index = indexOfKey(key);
        if (index > -1) {
            V value = (V) mValues[index];
            removeRow(index);
            return value;
        }
        return null;
    }

    public K removeByValue(V value) {
        int index = indexOfValue(value);
        if (index > -1) {
            K key = (K) mKeys[index];
            removeRow(index);
            return key;
        }
        return null;
    }

    //Passed elements should be direct indexes from arrays and they will left in result array

    public void removeRange(int fromIndex, int toIndex) {
        fromIndex++;
        int numMoved = mSize - toIndex;
        System.arraycopy(mKeys, toIndex, mKeys, fromIndex, numMoved);
        System.arraycopy(mValues, toIndex, mValues, fromIndex, numMoved);

        // clear to let GC do its work
        int newSize = mSize - (toIndex - fromIndex);
        for (int i = newSize; i < mSize; i++) {
            mKeys[i] = null;
            mValues[i] = null;
        }
        mSize = newSize;
        isKeysDirty = true;
        isValuesDirty = true;
    }

    private void removeRow(int index) {
        int numMoved = mSize - index - 1;
        if (numMoved > 0) {
            System.arraycopy(mKeys, index + 1, mKeys, index, numMoved);
            System.arraycopy(mValues, index + 1, mValues, index, numMoved);
        }
        --mSize;
        mKeys[mSize] = null;
        mValues[mSize] = null;
        isKeysDirty = true;
        isValuesDirty = true;
    }

    public V get(Object key) {
        if (key == null) {
            return null;
        }
        return findByKey(mKeys, key);
    }

    public K keyAt(int position) {
        return (K) mKeys[position];
    }

    public V valueAt(int position) {
        return (V) mValues[position];
    }

    public K lastKey() {
        if (mSize == 0) {
            return null;
        }
        return (K) mKeys[mSize - 1];
    }

    public V lastValue() {
        if (mSize == 0) {
            return null;
        }
        return (V) mValues[mSize - 1];
    }

    public boolean containsKey(K key) {
        if (key == null) {
            return false;
        }
        prepareSortedKeys();
        return Arrays.binarySearch(mSortedKeys, 0, mSize, key) > -1;
    }

    public int indexOfKey(K key) {
        for (int i = 0; i < mSize; i++) {
            if (mKeys[i].equals(key)) {
                return i;
            }
        }
        return -1;
    }

    public boolean containsValue(V value) {
        if (value == null) {
            return false;
        }
        prepareSortedValues();
        return Arrays.binarySearch(mSortedValues, 0, mSize, value) > -1;
    }

    public int indexOfValue(V value) {
        for (int i = 0; i < mSize; i++) {
            if (mValues[i].equals(value)) {
                return i;
            }
        }
        return -1;
    }

    public void clear() {
        mSize = 0;
        isKeysDirty = true;
        isValuesDirty = true;
        Arrays.fill(mKeys, null);
        Arrays.fill(mValues, null);
        Arrays.fill(mSortedKeys, null);
        Arrays.fill(mSortedValues, null);
    }

    private V findByKey(Object[] keys, Object key) {
        for (int i = 0; i < mSize; i++) {
            if (keys[i].equals(key)) {
                return (V) mValues[i];
            }
        }
        return null;
    }

    private void checkCapacity() {
        if (mKeys.length == mSize) {
            final int newSize = mSize + INCREMENT_CAPACITY;
            mKeys = incrementArray(mKeys, newSize);
            mValues = incrementArray(mValues, newSize);
        }
    }

    private void prepareSortedKeys() {
        if (isKeysDirty) {
            if (mSortedKeys.length <= mSize) {
                final int newSize = mSize + INCREMENT_CAPACITY;
                mSortedKeys = incrementArray(mSortedKeys, newSize);
            }
            System.arraycopy(mKeys, 0, mSortedKeys, 0, mKeys.length);
            Arrays.sort(mSortedKeys, 0, mSize);
            isKeysDirty = false;
        }
    }

    private void prepareSortedValues() {
        if (isValuesDirty) {
            if (mSortedValues.length <= mSize) {
                final int newSize = mSize + INCREMENT_CAPACITY;
                mSortedValues = incrementArray(mSortedValues, newSize);
            }
            System.arraycopy(mValues, 0, mSortedValues, 0, mKeys.length);
            Arrays.sort(mSortedValues, 0, mSize);
            isValuesDirty = false;
        }
    }

    private Object[] incrementArray(Object[] array, int newSize) {
        Object[] newArray = new Object[newSize];
        System.arraycopy(array, 0, newArray, 0, array.length);
        return newArray;
    }

}
