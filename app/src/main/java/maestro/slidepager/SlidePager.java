package maestro.slidepager;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.graphics.ColorUtils;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import android.support.v4.view.AbsSavedState;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ViewDragHelper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

/**
 * Created by maestro123 on 17.11.2016.
 */

public class SlidePager extends ViewGroup {

    public static final String TAG = SlidePager.class.getSimpleName();

    private static final int MIN_FLING_VELOCITY = 400; // dips per second

    private SlideAdapter mAdapter;
    private ViewDragHelper mDragHelper;

    private ValueAnimator mAnimator;

    private Transformer mTransformer = new ShadowTransformer((int) (255 * 0.8f));

    private int mCurrentPosition = -1;
    private int mPendingPosition = -1;

    private float mInitialMotionX;
    private float mInitialMotionY;

    private boolean mCanSlide = true;
    private boolean mIsUnableToDrag = false;
    private boolean mIsFirstLayout = false;
    private boolean mIsMoveToZero = false;

    public class Page {
        Fragment fragment;
        int ordinal;
    }

    private Stack<Runnable> mQueued = new Stack<>();

    public SlidePager(Context context) {
        this(context, null);
    }

    public SlidePager(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SlidePager(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        final float density = context.getResources().getDisplayMetrics().density;
        mDragHelper = ViewDragHelper.create(this, 0.5f, mDragCallback);
        mDragHelper.setMinVelocity(MIN_FLING_VELOCITY * density);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        final int childSize = getChildCount();
        if (childSize == 0) {
            return;
        }

        final int width = getWidth();
        final int height = getHeight();

        for (int i = 0; i < childSize; i++) {
            final View child = getChildAt(i);
            if (mIsFirstLayout && i == childSize - 1) {
                mIsFirstLayout = false;
                ViewCompat.offsetLeftAndRight(child, getMeasuredWidth());
                child.layout(child.getMeasuredWidth(), 0, child.getMeasuredWidth() + width, height);
            } else {
                child.layout(0, 0, width, height);
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        measureChildren(widthMeasureSpec, heightMeasureSpec);
        mCanSlide = getChildCount() > 1;
    }

    @Override
    protected boolean drawChild(Canvas canvas, View child, long drawingTime) {
        if (mTransformer != null) {
            float offset = 1f - (float) child.getLeft() / child.getMeasuredWidth();
            mTransformer.transform(canvas, child, offset);
        }
        return super.drawChild(canvas, child, drawingTime);
    }

    public class ShadowTransformer implements Transformer {

        private int mShadowAlpha;

        public ShadowTransformer(int alpha) {
            mShadowAlpha = alpha;
        }

        @Override
        public void transform(Canvas canvas, View child, float offset) {
            int alpha = (int) (mShadowAlpha * offset);
            canvas.drawColor(ColorUtils.setAlphaComponent(Color.BLACK, alpha));
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        final int action = MotionEventCompat.getActionMasked(ev);

        if (!mCanSlide || (mIsUnableToDrag && action != MotionEvent.ACTION_DOWN)) {
            mDragHelper.cancel();
            return super.onInterceptTouchEvent(ev);
        }

        if (action == MotionEvent.ACTION_CANCEL || action == MotionEvent.ACTION_UP) {
            mDragHelper.cancel();
            return false;
        }

        switch (action) {
            case MotionEvent.ACTION_DOWN: {
                mIsUnableToDrag = false;
                final float x = ev.getX();
                final float y = ev.getY();
                mInitialMotionX = x;
                mInitialMotionY = y;
                break;
            }
            case MotionEvent.ACTION_MOVE: {
                final float x = ev.getX();
                final float y = ev.getY();
                final float adx = Math.abs(x - mInitialMotionX);
                final float ady = Math.abs(y - mInitialMotionY);
                final int slop = mDragHelper.getTouchSlop();
                if (adx > slop && ady > adx) {
                    mDragHelper.cancel();
                    mIsUnableToDrag = true;
                    return false;
                }
            }
        }

        final boolean interceptForDrag = mDragHelper.shouldInterceptTouchEvent(ev);

        return interceptForDrag || (mAnimator != null && mAnimator.isRunning());
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (!mCanSlide) {
            return super.onTouchEvent(ev);
        }

        mDragHelper.processTouchEvent(ev);

        final int action = ev.getAction();
        boolean wantTouchEvents = true;

        switch (action & MotionEventCompat.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN: {
                final float x = ev.getX();
                final float y = ev.getY();
                mInitialMotionX = x;
                mInitialMotionY = y;
                break;
            }

            case MotionEvent.ACTION_UP: {
                final float x = ev.getX();
                final float y = ev.getY();
                final float dx = x - mInitialMotionX;
                final float dy = y - mInitialMotionY;
                final int slop = mDragHelper.getTouchSlop();
//                if (dx * dx + dy * dy < slop * slop
//                        && mDragHelper.isViewUnder(getChildAt(1), (int) x, (int) y)) {
//                    // Taps close a dimmed open pane.
////                    closePane(mSlideableView, 0);
//                    break;
//                }
                break;
            }
        }

        return wantTouchEvents;
    }

    public int getCurrentPosition() {
        return mCurrentPosition;
    }

    public void setAdapter(SlideAdapter adapter) {
        setAdapter(adapter, -1);
    }

    public void setAdapter(SlideAdapter adapter, int position) {
        if (mAdapter != null) {
            //TODO: destroy all current items
        }
        mCurrentPosition = position;
        mAdapter = adapter;
        if (mAdapter != null) {
            populateAdapter(-1);
        }
        requestLayout();
        invalidate();
    }

    private void populateAdapter(int position) {
        stopAnimator();
        Log.e(TAG, "setAdapter: " + mCurrentPosition);
        int childCount = mAdapter.getItemCount();
        if (childCount > 0) {
            mCurrentPosition = position == -1 ? childCount - 1 : position;
            mAdapter.startUpdate(this);
            if (mCurrentPosition > 0) {
                mAdapter.instantiateItem(this, mCurrentPosition - 1);
            }
            mAdapter.instantiateItem(this, mCurrentPosition);
            mAdapter.finishUpdate(this);
        }
    }

    private final class PositionRunnable implements Runnable {

        private int position;

        public PositionRunnable(int position) {
            this.position = position;
        }

        @Override
        public void run() {
            setCurrentPositionInternal(position);
        }
    }

    public void setCurrentPosition(int position) {
        submitQueue(new PositionRunnable(position));
    }

    private void submitQueue(Runnable runnable) {
        mQueued.push(runnable);
        nextQueue();
    }

    private void nextQueue() {
        Log.e(TAG, "nextQueue: " + isQueueLocked);
        if (!isQueueLocked && !mQueued.isEmpty()) {
            Log.e(TAG, "nextQueue: " + mQueued.peek());
            mQueued.pop().run();
        }
    }


    private boolean isQueueLocked = false;

    public void setQueueLocked(boolean locked) {
        isQueueLocked = locked;
        if (!locked) {
            nextQueue();
        }
    }

    private void decreaseCurrent() {
        mCurrentPosition--;
        for (Runnable runnable : mQueued) {
            if (runnable instanceof PositionRunnable) {
                ((PositionRunnable) runnable).position--;
            }
        }
    }

    private void setCurrentPositionInternal(int position) {
        if (mAdapter == null || position == mCurrentPosition || position == mPendingPosition) {
            return;
        }
        setQueueLocked(true);
        if (position > mCurrentPosition) {
            mAdapter.startUpdate(this);
            if (mCurrentPosition > 0) {
                int previousPosition = mCurrentPosition - 1;
                mAdapter.destroyItem(this, previousPosition, mAdapter.getFragment(previousPosition, getId()), true);
            }

            mPendingPosition = position;
            mIsFirstLayout = true;

            mAdapter.instantiateItem(this, position);
            mAdapter.finishUpdate(this);

            post(new Runnable() {
                @Override
                public void run() {
                    if (mPendingPosition != -1) {
                        mCurrentPosition = mPendingPosition;
                        mPendingPosition = -1;
                        performAnimate(new Runnable() {
                            @Override
                            public void run() {
                                setQueueLocked(false);
                            }
                        }, true);
                    }
                }
            });
        } else if (position == mCurrentPosition - 1) {
            final int currentPosition = mCurrentPosition;
            decreaseCurrent();
            performAnimate(new Runnable() {
                @Override
                public void run() {
                    destroyItemNotSoft(currentPosition);
                    setQueueLocked(false);
                }
            }, false);
        } else {
            decreaseCurrent();
            destroyItemNotSoft(mCurrentPosition);
            performAnimate(new Runnable() {
                @Override
                public void run() {
                    if (mCurrentPosition > 0) {
                        mIsMoveToZero = true;
                        mAdapter.startUpdate(SlidePager.this);
                        mAdapter.instantiateItem(SlidePager.this, mCurrentPosition - 1);
                        mAdapter.finishUpdate(SlidePager.this);
                    }
                    setQueueLocked(false);
                }
            }, false);
        }
    }

    private void performAnimate(final Runnable endRunnable, boolean open) {
        final View child = getChildAt(getChildCount() - 1);
        mAnimator = ValueAnimator.ofFloat(open ? 1f : 0f, open ? 0f : 1f);
        mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                ViewCompat.offsetLeftAndRight(child, (int) (getMeasuredWidth() * (float) valueAnimator.getAnimatedValue()) - child.getLeft());
                invalidate();
            }
        });
        mAnimator.addListener(new AnimatorListenerAdapter() {

            boolean isCanceled = false;

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                mCanSlide = false;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mCanSlide = true;
                if (!isCanceled) {
                    if (endRunnable != null) {
                        endRunnable.run();
                    }
                    if (mCurrentPosition > -1) {
                        mAdapter.setPrimaryItem(SlidePager.this, mCurrentPosition,
                                mAdapter.getFragment(mCurrentPosition, getId()));
                    }
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                super.onAnimationCancel(animation);
                isCanceled = true;
            }
        });
        mAnimator.start();
    }

    @Override
    protected LayoutParams generateDefaultLayoutParams() {
        return new MarginLayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
    }

    @Override
    public LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new MarginLayoutParams(getContext(), attrs);
    }

    @Override
    protected LayoutParams generateLayoutParams(LayoutParams p) {
        return new MarginLayoutParams(p);
    }

    @Override
    protected boolean checkLayoutParams(LayoutParams p) {
        return p instanceof MarginLayoutParams;
    }

    @Override
    public void computeScroll() {
        if (mDragHelper.continueSettling(true)) {
            if (!mCanSlide) {
                mDragHelper.abort();
                return;
            }
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    @Override
    public Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SlidePager.SavedState ss = new SlidePager.SavedState(superState);
        ss.position = mCurrentPosition;
        if (mAdapter != null) {
            Bundle bundle = new Bundle();
            mAdapter.saveState(bundle);
            ss.adapterState = bundle;
        }
        return ss;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        if (!(state instanceof SlidePager.SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }

        SlidePager.SavedState ss = (SlidePager.SavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());

        if (mAdapter != null) {
            mCurrentPosition = ss.position;
            mAdapter.restoreState((Bundle) ss.adapterState);
        }
    }

    private void stopAnimator() {
        if (mAnimator != null && mAnimator.isRunning()) {
            mAnimator.cancel();
            mIsFirstLayout = false;
            mPendingPosition = -1;
        }
    }

    public static class SavedState extends AbsSavedState {
        int position;
        Parcelable adapterState;
        ClassLoader loader;

        public SavedState(Parcelable superState) {
            super(superState);
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(position);
            out.writeParcelable(adapterState, flags);
        }

        @Override
        public String toString() {
            return "SlidePager.SavedState{"
                    + Integer.toHexString(System.identityHashCode(this))
                    + " position=" + position + "}";
        }

        public static final Creator<SavedState> CREATOR = ParcelableCompat.newCreator(
                new ParcelableCompatCreatorCallbacks<SavedState>() {
                    @Override
                    public SlidePager.SavedState createFromParcel(Parcel in, ClassLoader loader) {
                        return new SlidePager.SavedState(in, loader);
                    }

                    @Override
                    public SlidePager.SavedState[] newArray(int size) {
                        return new SlidePager.SavedState[size];
                    }
                });

        SavedState(Parcel in, ClassLoader loader) {
            super(in, loader);
            if (loader == null) {
                loader = getClass().getClassLoader();
            }
            position = in.readInt();
            adapterState = in.readParcelable(loader);
            this.loader = loader;
        }
    }

    private final ViewDragHelper.Callback mDragCallback = new ViewDragHelper.Callback() {

        boolean swapAfterIdle = false;

        @Override
        public boolean tryCaptureView(View child, int pointerId) {
            return getChildCount() > 1 && child == getChildAt(getChildCount() - 1);
        }

        @Override
        public void onViewPositionChanged(View changedView, int left, int top, int dx, int dy) {
            super.onViewPositionChanged(changedView, left, top, dx, dy);
            invalidate();
        }

        @Override
        public int clampViewPositionHorizontal(View child, int left, int dx) {
            final MarginLayoutParams lp = (MarginLayoutParams) child.getLayoutParams();

            final int newLeft;
            if (isLayoutRtlSupport()) {
                int startBound = getWidth()
                        - (getPaddingRight() + lp.rightMargin + child.getWidth());
                int endBound = getWidth();
                newLeft = Math.max(Math.min(left, startBound), endBound);
            } else {
                int startBound = getPaddingLeft() + lp.leftMargin;
                int endBound = getWidth();
                newLeft = Math.min(Math.max(left, startBound), endBound);
            }
            return newLeft;
        }

        @Override
        public int getViewHorizontalDragRange(View child) {
            return child.getMeasuredWidth();
        }

        @Override
        public void onViewReleased(View releasedChild, float xvel, float yvel) {
            if (xvel > 0 || releasedChild.getLeft() >= (releasedChild.getMeasuredWidth() >> 1)) {
                swapAfterIdle = true;
                mDragHelper.settleCapturedViewAt(releasedChild.getMeasuredWidth(), 0);
            } else {
                mDragHelper.settleCapturedViewAt(0, 0);
            }
            invalidate();
        }

        @Override
        public void onViewDragStateChanged(int state) {
            super.onViewDragStateChanged(state);
            if (state == ViewDragHelper.STATE_IDLE) {
                if (swapAfterIdle) {
                    final int currentPosition = mCurrentPosition;
                    decreaseCurrent();
                    destroyItemNotSoft(currentPosition);
                }
                swapAfterIdle = false;
            } else if (state == ViewDragHelper.STATE_DRAGGING) {
                swapAfterIdle = false;
            }
        }
    };

    @Override
    public void addView(View child, int index, LayoutParams params) {
        if (mIsMoveToZero) {
            super.addView(child, 0, params);
            mIsMoveToZero = false;
        } else {
            super.addView(child, index, params);
        }
    }

    @Override
    public boolean canScrollHorizontally(int direction) {
        return mCanSlide && direction < 0 || super.canScrollHorizontally(direction);
    }

    private void destroyItemNotSoft(int position) {
        mAdapter.startUpdate(this);
        mAdapter.destroyItem(this, position, mAdapter.getFragment(position, getId()), false);
        if (mCurrentPosition > 0) {
            mIsMoveToZero = true;
            mAdapter.instantiateItem(this, mCurrentPosition - 1);
        }
        mAdapter.finishUpdate(this);
        invalidate();
    }

    boolean isLayoutRtlSupport() {
        return ViewCompat.getLayoutDirection(this) == ViewCompat.LAYOUT_DIRECTION_RTL;
    }

    public interface Transformer {
        void transform(Canvas canvas, View child, float offset);
    }

    public abstract static class SimpleSlideAdapter<ObjectType extends Serializable, FragmentType extends Fragment> extends SlideAdapter<FragmentType> {

        private static final String SAVED_SERIALIZABLE = "savedSerializable";

        private SlidePager mPager;
        private final ArrayList<ObjectType> mItems = new ArrayList<>();

        public SimpleSlideAdapter(SlidePager pager, FragmentManager fm) {
            super(fm);
            mPager = pager;
        }

        @Override
        public Fragment getItem(int position) {
            return getItem(mItems.get(position), position);
        }

        @Override
        public Object getItemKey(int position) {
            return getItemKey(mItems.get(position), position);
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        @Override
        public void onItemDestroyed(int position, boolean soft) {
            if (!soft) {
                mItems.remove(position);
            }
        }

        @Override
        public void saveState(Bundle state) {
            super.saveState(state);
            state.putSerializable(SAVED_SERIALIZABLE, mItems);
        }

        @Override
        public void restoreState(Bundle bundle) {
            super.restoreState(bundle);
            mItems.addAll((Collection<? extends ObjectType>) bundle.getSerializable(SAVED_SERIALIZABLE));
        }

        public void add(final ObjectType objectType) {
            mPager.submitQueue(new Runnable() {
                @Override
                public void run() {
                    if (!mItems.contains(objectType)) {
                        mItems.add(objectType);
                        mPager.setCurrentPosition(mItems.indexOf(objectType));
                    }
                }
            });
        }

        public void remove(final ObjectType objectType) {
            mPager.submitQueue(new Runnable() {
                @Override
                public void run() {
                    final int index = mItems.indexOf(objectType);
                    if (index > -1) {
                        final int position = mPager.mCurrentPosition;
                        if (index == position) {
                            mPager.setCurrentPosition(position - 1);
                        } else if (index == position - 1) {
                            mPager.decreaseCurrent();
                            mPager.destroyItemNotSoft(position - 1);
                        } else {
                            mPager.decreaseCurrent();
                            mItems.remove(index);
                        }
                    }
                }
            });
        }

        public void update(final List<ObjectType> items) {
            mPager.submitQueue(new Runnable() {
                @Override
                public void run() {
                    ObjectType currentItem = mPager.mCurrentPosition > 0 ? mItems.get(mPager.mCurrentPosition) : null;
                    mItems.clear();
                    mItems.addAll(items);
                    int newIndex = mItems.indexOf(currentItem);
                    if (newIndex == -1) {
                        newIndex = mItems.size() - 1;
                    }
                    mPager.setCurrentPosition(newIndex);
                }
            });
        }

        public ObjectType getCurrent() {
            return mPager.mCurrentPosition == -1 ? null : mItems.get(mPager.mCurrentPosition);
        }

        public ObjectType getObject(int position) {
            return position > -1 && position < getItemCount() ? mItems.get(position) : null;
        }

        public abstract FragmentType getItem(ObjectType objectType, int position);

        public abstract Object getItemKey(ObjectType objectType, int position);

    }

    public static abstract class SlideAdapter<FragmentType extends Fragment> {

        private final FragmentManager mFragmentManager;

        private FragmentType mCurrentPrimaryItem = null;
        private FragmentTransaction mCurTransaction = null;
        private HashMap<Object, FragmentType> mFragments = new HashMap<>();
        private HashMap<Object, Fragment.SavedState> mSavedSates = new HashMap<>();

        public SlideAdapter(FragmentManager fm) {
            this.mFragmentManager = fm;
        }

        public abstract Fragment getItem(int var1);

        public abstract Object getItemKey(int position);

        public abstract int getItemCount();

        public abstract void onItemDestroyed(int position, boolean soft);

        public Object instantiateItem(ViewGroup container, int position) {
            Object key = getItemKey(position);

            if (mFragments.size() > position) {
                Fragment f = mFragments.get(key);
                if (f != null) {
                    return f;
                }
            }

            String tag = makeFragmentName(container.getId(), key);
            Fragment fragment = this.mFragmentManager.findFragmentByTag(tag);
            if (this.mCurTransaction == null) {
                this.mCurTransaction = this.mFragmentManager.beginTransaction();
            }
            if (fragment == null) {
                fragment = this.getItem(position);
                this.mCurTransaction.add(container.getId(), fragment, tag);
            }
            if (this.mSavedSates.containsKey(key) && !fragment.isAdded()) {
                Fragment.SavedState fss = this.mSavedSates.get(key);
                if (fss != null) {
                    fragment.setInitialSavedState(fss);
                }
            }
            onItemInstantiated((FragmentType) fragment, position);
            fragment.setMenuVisibility(false);
            fragment.setUserVisibleHint(false);
            mFragments.put(key, (FragmentType) fragment);
            return fragment;
        }

        public void destroyItem(ViewGroup container, int position, Object object, boolean soft) {
            Fragment fragment = (Fragment) object;
            Object key = getKeyFromValue(mFragments, fragment);
            if (this.mCurTransaction == null) {
                this.mCurTransaction = this.mFragmentManager.beginTransaction();
            }
            this.mCurTransaction.remove(fragment);
            if (position < getItemCount() && fragment.isAdded() && fragment.getRetainInstance()) {
                this.mSavedSates.put(key, this.mFragmentManager.saveFragmentInstanceState(fragment));
            }
            mFragments.remove(key);
            onItemDestroyed(position, soft);
        }

        public Object getKeyFromValue(Map hm, Object value) {
            for (Object o : hm.keySet()) {
                if (hm.get(o).equals(value)) {
                    return o;
                }
            }
            return null;
        }

        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            FragmentType fragment = (FragmentType) object;
            if (fragment != this.mCurrentPrimaryItem) {
                if (this.mCurrentPrimaryItem != null) {
                    this.mCurrentPrimaryItem.setMenuVisibility(false);
                    this.mCurrentPrimaryItem.setUserVisibleHint(false);
                }

                if (fragment != null) {
                    fragment.setMenuVisibility(true);
                    fragment.setUserVisibleHint(true);
                }

                this.mCurrentPrimaryItem = fragment;
                onPrimaryItemChanged(mCurrentPrimaryItem);
            }
        }

        public FragmentType getCurrentPrimaryItem() {
            return mCurrentPrimaryItem;
        }

        public void startUpdate(ViewGroup container) {
            if (this.mCurTransaction == null) {
                this.mCurTransaction = this.mFragmentManager.beginTransaction();
            }
        }

        public void finishUpdate(ViewGroup container) {
            if (this.mCurTransaction != null && mFragmentManager != null && !mFragmentManager.isDestroyed()) {
                this.mCurTransaction.commitAllowingStateLoss();
                this.mCurTransaction = null;
            }
        }

        public void onItemInstantiated(FragmentType fragment, int position) {
        }

        public void onPrimaryItemChanged(FragmentType primaryItem) {
        }

        public void saveState(Bundle state) {
            Set<Object> keyIterator = mFragments.keySet();
            for (Object key : keyIterator) {
                Fragment f = mFragments.get(key);
                if (f != null && f.isAdded() && f.getRetainInstance()) {
                    this.mSavedSates.put(key, this.mFragmentManager.saveFragmentInstanceState(f));
                }
            }
            if (this.mSavedSates.size() > 0) {
                state.putSerializable("states", mSavedSates);
            }
        }

        public void restoreState(Bundle bundle) {
            if (bundle != null) {
                mSavedSates = (HashMap<Object, Fragment.SavedState>) bundle.getSerializable("states");
                if (mSavedSates == null) {
                    mSavedSates = new HashMap<>();
                }
            }
        }

        public FragmentType getFragment(int position, int containerId) {
            String tag = makeFragmentName(containerId, getItemKey(position));
            return (FragmentType) mFragmentManager.findFragmentByTag(tag);
        }

        public static String makeFragmentName(int viewId, Object key) {
            return "android:switcher:" + viewId + "(" + key + ")";
        }


    }

}
