package maestro.slidepager;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.graphics.ColorUtils;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import android.support.v4.view.AbsSavedState;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Scroller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by maestro123 on 21.11.2016.
 */

public class StackPager extends ViewGroup {

    public static final String TAG = StackPager.class.getSimpleName();

    private final ArrayList<PageInfo> mInfos = new ArrayList<>();
    private final ArrayList<View> mAnimatingViews = new ArrayList<>();
    private final ArrayList<PageInfo> mPendingInfos = new ArrayList<>();

    private final ArrayList<Serializable> mJumpPending = new ArrayList<>();

    private final ArrayList<Serializable> mPendingAnimateRemove = new ArrayList<>();
    private final ArrayList<Serializable> mPendingAnimateInserts = new ArrayList<>();

    private DragHelper mDragHelper;
    private Transformation mTransformation;

    private StackPageAdapter mAdapter;

    private float mInitialMotionX;
    private float mInitialMotionY;

    private boolean mCanSlide = true;
    private boolean mIsUnableToDrag = false;
    private boolean isFirstDraggable = false;

    public StackPager(Context context) {
        this(context, null);
    }

    public StackPager(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public StackPager(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        final float density = context.getResources().getDisplayMetrics().density;
        mDragHelper = DragHelper.create(this, 0.5f, mDragCallback);
        mDragHelper.setMinVelocity(400 * density);

        mTransformation = new ShadowTransformation((int) (255 * 0.8f));
        setChildrenDrawingOrderEnabled(true);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        final int count = getChildCount();
        if (count == 0) {
            return;
        }
        final int width = getWidth();
        final int height = getHeight();

        for (int i = 0; i < count; i++) {
            getChildAt(i).layout(0, 0, width, height);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        measureChildren(widthMeasureSpec, heightMeasureSpec);
        mCanSlide = getChildCount() > 1;
    }

    @Override
    protected boolean drawChild(Canvas canvas, View child, long drawingTime) {
        if (mTransformation != null) {
            float offset = 1f - (Math.abs(child.getTranslationX()) / getMeasuredWidth());
            mTransformation.transform(canvas, child, offset);
        }
        return super.drawChild(canvas, child, drawingTime);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        final int action = MotionEventCompat.getActionMasked(ev);

        if (!mCanSlide || (mIsUnableToDrag && action != MotionEvent.ACTION_DOWN) || haveAnimatingView()) {
            mDragHelper.cancel();
            return super.onInterceptTouchEvent(ev);
        }

        if (action == MotionEvent.ACTION_CANCEL || action == MotionEvent.ACTION_UP) {
            mDragHelper.cancel();
            return false;
        }

        switch (action) {
            case MotionEvent.ACTION_DOWN: {
                mIsUnableToDrag = false;
                final float x = ev.getX();
                final float y = ev.getY();
                mInitialMotionX = x;
                mInitialMotionY = y;
                break;
            }
            case MotionEvent.ACTION_MOVE: {
                final float x = ev.getX();
                final float y = ev.getY();
                final float adx = Math.abs(x - mInitialMotionX);
                final float ady = Math.abs(y - mInitialMotionY);
                final int slop = mDragHelper.getTouchSlop();
                if (adx > slop && ady > adx) {
                    mDragHelper.cancel();
                    mIsUnableToDrag = true;
                    return false;
                }
            }
        }

        final boolean interceptForDrag = mDragHelper.shouldInterceptTouchEvent(ev);

        //TODO: check animations?

        return interceptForDrag;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (!mCanSlide || haveAnimatingView()) {
            return super.onTouchEvent(ev);
        }
        mDragHelper.processTouchEvent(ev);
        return true;
    }

    @Override
    public void computeScroll() {
        if (mDragHelper.continueSettling(true)) {
            if (!mCanSlide) {
                mDragHelper.abort();
                return;
            }
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    @Override
    protected LayoutParams generateDefaultLayoutParams() {
        return new StackLayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
    }

    @Override
    public LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new StackLayoutParams(getContext(), attrs);
    }

    @Override
    protected LayoutParams generateLayoutParams(LayoutParams p) {
        return new StackLayoutParams(p);
    }

    @Override
    protected boolean checkLayoutParams(LayoutParams p) {
        return p instanceof StackLayoutParams;
    }

    @Override
    public void addView(View child, int index, LayoutParams params) {
        super.addView(child, index, params);
        PageInfo info = findPendingInfo(child);
        Log.e(TAG, "addView: " + info);
        if (info != null) {
            mInfos.add(info);
            Log.e(TAG, "mPendingInfos.size before: " + mPendingInfos.size());
            mPendingInfos.remove(info);
            Log.e(TAG, "mPendingInfos.size after: " + mPendingInfos.size());
        } else {
            info = findInfo(child);
        }
        invalidateDrawOrder();
        if (info != null && mPendingAnimateInserts.contains(info.key)) {
            child.setTranslationX(getMeasuredWidth());
            animateInsert(child);
            mPendingAnimateInserts.remove(info.key);
        }
    }

    @Override
    public void removeView(View view) {
        super.removeView(view);
        final StackLayoutParams params = (StackLayoutParams) view.getLayoutParams();
        if (params.mAnimateState != AnimateState.NONE && params.mAnimateState != AnimateState.DRAG) {
            params.setExternalRemoved(true);
        }
        mAnimatingViews.remove(view);
        PageInfo info = findInfo(view);
        Log.e(TAG, "onViewRemoved: " + info);
        if (info != null) {
            mInfos.remove(info);
            mPendingInfos.remove(info);
            mJumpPending.remove(info.key);
            mPendingAnimateRemove.remove(info.key);
            mPendingAnimateInserts.remove(info.key);
        }
        invalidateDrawOrder();
        populateAdapter();
        Log.e(TAG, "fragments count: " + mAdapter.mFragmentManager.getFragments().size());
    }

    @Override
    public Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState ss = new SavedState(superState);
        ss.adapterKeys = new ArrayList<>();
        if (mAdapter != null) {
            ss.adapterState = mAdapter.saveState();
            for (PageInfo info : mInfos) {
                ss.adapterKeys.add(info.key);
            }
        }
        return ss;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (!(state instanceof SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }

        SavedState ss = (SavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());

        if (mAdapter != null) {
            mAdapter.restoreState(ss.adapterState, ss.loader);
            if (ss.adapterKeys.size() > 0) {
                for (Serializable key : ss.adapterKeys) {
                    if (mAdapter.mItems.containsKey(key)) {
                        PageInfo info = new PageInfo();
                        info.key = key;
                        info.fragment = mAdapter.findFragment(key, getId());
                        mInfos.add(info);
                    } else {
                        mAdapter.destroyItem(key, getId());
                    }
                }
                invalidateDrawOrder();
                mAdapter.finishUpdate();
            }
        }
    }

    public void setAdapter(StackPageAdapter adapter) {
        if (mAdapter != null) {
            mAdapter.setPagerObserver(null);
            if (adapter == null) {
                final int count = getChildCount();
                for (int i = 0; i < count; i++) {
                    final View view = getChildAt(i);
                    final PageInfo info = findInfo(view);
                    mAdapter.destroyItem(info.key, getId());
                }
            }
            clean();
            removeAllViews();
            mAdapter.finishUpdate();
        }
        mAdapter = adapter;
        if (mAdapter != null) {
            mAdapter.setPagerObserver(mDataObserver);
        }
        populateAdapter();
    }

    private void populateAdapter() {
        final int count = getChildCount();
        if (mAdapter != null) {
            Serializable topKey = mAdapter.topKey();
            Serializable secondKey = mAdapter.previousKey();
            Log.e(TAG, "populateAdapter: " + topKey + ", " + secondKey + ", infos: " + mInfos.size() + ", count: " + count);
            if (count > 0) {
                for (int i = 0; i < count; i++) {
                    final View child = getChildAt(i);
                    final PageInfo info = findInfo(child);
                    final StackLayoutParams params = (StackLayoutParams) child.getLayoutParams();
                    if (info.key.equals(topKey) || info.key.equals(secondKey)) {
                        if (params.mAnimateState == AnimateState.HIDE && !mJumpPending.contains(info.key)) {
                            mPendingAnimateRemove.remove(info.key);
                            mPendingAnimateInserts.add(info.key);
                            params.setPendingRemove(false);
                            params.animateInsert(child);
                        }
                        if (info.key.equals(topKey)) {
                            topKey = null;
                        } else {
                            secondKey = null;
                        }
                    } else {
                        if (params.mAnimateState != AnimateState.NONE || !isCoveredByNext(info)) {
                            Log.e(TAG, "setPendingRemove: " + info + ", state: " + params.mAnimateState + ", isCoveredByNext: " + isCoveredByNext(info) + ", view: " + child);
                            params.setPendingRemove(true);
                        } else {
                            destroy(info, "populateAdapter");
                        }
                    }
                }
            }
            if (secondKey != null) {
                instantiateItem(secondKey);
            }
            if (topKey != null) {
                instantiateItem(topKey);
            }
            mAdapter.finishUpdate();
            invalidateDrawOrder();
        }
    }

    private void ensurePending() {
        final int count = getChildCount();
        for (int i = 0; i < count; i++) {
            final View child = getChildAt(i);
            final PageInfo info = findInfo(child);
            final StackLayoutParams params = (StackLayoutParams) child.getLayoutParams();
            if (params.isPendingRemove && (params.mAnimateState == AnimateState.NONE || isCoveredByNext(info))) {
                destroy(info, "ensurePending");
            }
        }
        mAdapter.finishUpdate();
    }

    private boolean isCoveredByNext(PageInfo info) {
        PageInfo nextInfo = null;
        for (PageInfo pageInfo : mInfos) {
            if (pageInfo.drawOrder > info.drawOrder && (nextInfo == null || nextInfo.drawOrder > pageInfo.drawOrder)) {
                nextInfo = pageInfo;
            }
        }
        if (nextInfo != null) {
            final View child = getChildAt(nextInfo.childIndex);
            return child.getLeft() == 0 && child.getTranslationX() == 0;
        }
        return true;
    }

    private void executeRemove(PageInfo info) {
        Log.e(TAG, "execute remove : " + info);
        mAdapter.removeByKey(info.key);
    }

    private void destroy(PageInfo info, String who) {
        Log.e(TAG, "destroy: " + info.key + ", who: " + who);
        mAdapter.destroyItem(info.key, getId());
        mPendingInfos.remove(info);
    }

    private void instantiateItem(Serializable key) {
        PageInfo pageInfo = new PageInfo();
        pageInfo.key = key;
        Log.e(TAG, "instantiate item: " + key + ", isPending: " + mPendingInfos.contains(pageInfo));
        if (!mPendingInfos.contains(pageInfo)) {
            pageInfo.fragment = mAdapter.instantiateItem(key, getId());
            mPendingInfos.add(pageInfo);
        }
    }

    public PageInfo findInfo(int childIndex) {
        for (PageInfo info : mInfos) {
            if (info.childIndex == childIndex) {
                return info;
            }
        }
        return null;
    }

    public PageInfo findInfo(Fragment fragment) {
        for (PageInfo info : mInfos) {
            if (info.fragment.equals(fragment)) {
                return info;
            }
        }
        return null;
    }

    public PageInfo findInfo(Serializable key) {
        for (PageInfo info : mInfos) {
            if (info.key.equals(key)) {
                return info;
            }
        }
        return null;
    }

    public PageInfo findInfo(View view) {
        for (PageInfo info : mInfos) {
            if (view == info.fragment.getView()) {
                return info;
            }
        }
        return null;
    }

    public View findChild(PageInfo info) {
        final int count = getChildCount();
        for (int i = 0; i < count; i++) {
            final View child = getChildAt(i);
            if (info.fragment.getView() == child) {
                return child;
            }
        }
        return null;
    }

    public PageInfo findPendingInfo(View view) {
        for (PageInfo info : mPendingInfos) {
            if (view == info.fragment.getView()) {
                return info;
            }
        }
        return null;
    }

    public boolean haveAnimatingView() {
        return mAnimatingViews.size() > 0;
    }

    private void clean() {
        mInfos.clear();
        mPendingInfos.clear();
        mAnimatingViews.clear();
        mPendingAnimateRemove.clear();
        mPendingAnimateInserts.clear();
    }

    @Override
    protected int getChildDrawingOrder(int childCount, int i) {
        PageInfo pageInfo = findInfo(i);
        if (pageInfo != null) {
            return pageInfo.drawOrder;
        }
        //can happen after recreate without adapter?
        return i;
    }

    @Override
    public boolean canScrollHorizontally(int direction) {
        return mCanSlide && direction < 0 || super.canScrollHorizontally(direction);
    }

    private void invalidateDrawOrder() {
        for (PageInfo info : mInfos) {
            info.adapterIndex = mAdapter.indexOfKey(info.key);
        }

        Collections.sort(mInfos, mPageComparator);

        final int count = getChildCount();
        int drawOrder = 0;
        for (PageInfo info : mInfos) {
            for (int i = 0; i < count; i++) {
                if (info.fragment.getView() == getChildAt(i)) {
                    info.drawOrder = drawOrder;
                    info.childIndex = i;
                    drawOrder++;
                    break;
                }
            }
        }
        Log.e(TAG, "invalidate draw order: " + mInfos.size() + ", count: " + count);
    }

    private final Comparator<PageInfo> mPageComparator = new Comparator<PageInfo>() {
        @Override
        public int compare(PageInfo p1, PageInfo p2) {
            return Integer.compare(p1.adapterIndex, p2.adapterIndex);
        }
    };

    private final StackPageAdapter.StackAdapterObserver mDataObserver = new StackPageAdapter.StackAdapterObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            populateAdapter();
        }

        @Override
        public void onInsert(Serializable key, Object item) {
            super.onInsert(key, item);
            mJumpPending.remove(key);
        }
    };

    private final DragHelper.Callback mDragCallback = new DragHelper.Callback() {

        PageInfo mDragPage;
        View mCurrentView;
        boolean swapAfterIdle = false;

        @Override
        public boolean tryCaptureView(View child, int pointerId) {
            if (isFirstDraggable || getChildCount() > 1) {
                mCurrentView = child;
                mDragPage = findInfo(child);
                Log.e(TAG, "drag page: " + mDragPage);
                return true;
            }
            return false;
        }

        @Override
        public int getOrderedChildIndex(int index) {
            return getChildDrawingOrder(getChildCount(), index);
        }

        @Override
        public void onViewPositionChanged(View changedView, int left, int top, int dx, int dy) {
            super.onViewPositionChanged(changedView, left, top, dx, dy);
            invalidate();
        }

        @Override
        public int clampViewPositionHorizontal(View child, int left, int dx) {
            final MarginLayoutParams lp = (MarginLayoutParams) child.getLayoutParams();

            final int newLeft;
            if (isLayoutRtlSupport()) {
                int startBound = getWidth()
                        - (getPaddingRight() + lp.rightMargin + child.getWidth());
                int endBound = getWidth();
                newLeft = Math.max(Math.min(left, startBound), endBound);
            } else {
                int startBound = getPaddingLeft() + lp.leftMargin;
                int endBound = getWidth();
                newLeft = Math.min(Math.max(left, startBound), endBound);
            }
            return newLeft;
        }

        @Override
        public int getViewHorizontalDragRange(View child) {
            return child.getMeasuredWidth();
        }

        @Override
        public void onViewReleased(View releasedChild, float xvel, float yvel) {
            if (xvel > 0 || releasedChild.getLeft() >= (releasedChild.getMeasuredWidth() >> 1)) {
                swapAfterIdle = true;
                mDragHelper.settleCapturedViewAt(releasedChild.getMeasuredWidth(), 0);
                setAnimationState(releasedChild, AnimateState.HIDE, "drag state RELEASE HIDE");
            } else {
                mDragHelper.settleCapturedViewAt(0, 0);
                setAnimationState(releasedChild, AnimateState.SHOW, "drag state RELEASE SHOW");
            }
            invalidate();
        }

        @Override
        public void onViewDragStateChanged(int state) {
            super.onViewDragStateChanged(state);
            Log.e(TAG, "onViewDragStateChanged: " + state);
            if (state == DragHelper.STATE_IDLE) {
                setAnimationState(findChild(mDragPage), AnimateState.NONE, "drag state IDLE");
                if (swapAfterIdle) {
                    executeRemove(mDragPage);
                }
                mDragPage = null;
                swapAfterIdle = false;
            } else if (state == DragHelper.STATE_DRAGGING) {
                setAnimationState(findChild(mDragPage), AnimateState.DRAG, "drag state DRAGGING");
                swapAfterIdle = false;
            }
        }

        boolean isLayoutRtlSupport() {
            return ViewCompat.getLayoutDirection(StackPager.this) == ViewCompat.LAYOUT_DIRECTION_RTL;
        }

    };

    private void setAnimationState(View view, AnimateState state, String who) {
        Log.e(TAG, "setAnimationState: " + view + ", " + state + ", who: " + who);
        if (view != null) {
            ((StackLayoutParams) view.getLayoutParams()).mAnimateState = state;
            if (state == AnimateState.NONE || state == AnimateState.DRAG) {
                mAnimatingViews.remove(view);
                ensurePending();
            } else if (!mAnimatingViews.contains(view)) {
                mAnimatingViews.add(view);
            }
        }
    }

    public void animatedAdd(Serializable item) {
        Serializable key = mAdapter.keyFor(item);
        if (!mPendingAnimateInserts.contains(key)) {
            mPendingAnimateInserts.add(key);
            mAdapter.add(item);
        }
    }

    public void animatedRemove(Serializable item) {
        animatedRemoveByKey(mAdapter.keyFor(item));
    }

    public void animatedRemoveByKey(Serializable key) {
        if (!mPendingAnimateRemove.contains(key)) {
            mPendingAnimateRemove.add(key);
            PageInfo info = findInfo(key);
            if (info != null && (key.equals(mAdapter.topKey()) || !isCoveredByNext(info))) {
                animateRemove(getChildAt(info.childIndex));
            } else {
                executeRemove(info);
            }
        }
    }

    public void jumpOrAdd(Serializable item) {
        Serializable key = mAdapter.keyFor(item);
        if (mAdapter.indexOfKey(key) != -1) {
            animatedJumpByKey(key);
        } else {
            animatedAdd(item);
        }
    }

    public void animatedJump(Serializable item) {
        animatedJumpByKey(mAdapter.keyFor(item));
    }

    public void animatedJumpByKey(Serializable key) {
        final int size = mAdapter.mItems.size();
        final int newIndex = mAdapter.indexOfKey(key);
        if (newIndex == -1 || size == 1 || newIndex == size - 1) {
            return;
        }
        Serializable topKey = mAdapter.topKey();
        mAdapter.mItems.removeRange(newIndex, size - 1);
        mJumpPending.add(topKey);
        populateAdapter();
        animatedRemoveByKey(topKey);
    }

    private void animateInsert(final View view) {
        StackLayoutParams params = (StackLayoutParams) view.getLayoutParams();
        params.animateInsert(view);
    }

    private void animateRemove(final View view) {
        StackLayoutParams params = (StackLayoutParams) view.getLayoutParams();
        params.animateRemove(view);
    }

    public interface Transformation {
        void transform(Canvas canvas, View child, float offset);
    }

    public static class ShadowTransformation implements Transformation {

        private int mShadowAlpha;

        public ShadowTransformation(int alpha) {
            mShadowAlpha = alpha;
        }

        @Override
        public void transform(Canvas canvas, View child, float offset) {
            int alpha = (int) (mShadowAlpha * offset);
            canvas.drawColor(ColorUtils.setAlphaComponent(Color.BLACK, alpha));
        }
    }

    private enum AnimateState {
        NONE, SHOW, HIDE, DRAG
    }

    private class StackLayoutParams extends MarginLayoutParams {

        private Scroller mScroller;

        private View mTargetView;
        private AnimateState mAnimateState = AnimateState.NONE;

        private float mTargetTranslation;

        private boolean isPendingRemove;
        private boolean isExternalRemoved;

        public StackLayoutParams(LayoutParams source) {
            super(source);
        }

        public StackLayoutParams(MarginLayoutParams source) {
            super(source);
        }

        public StackLayoutParams(int width, int height) {
            super(width, height);
        }

        public StackLayoutParams(Context c, AttributeSet attrs) {
            super(c, attrs);
        }

        void animateInsert(View view) {
            setAnimationState(view, AnimateState.SHOW, "animate INSERT");
            mTargetTranslation = 0f;
            animate(view);
        }

        void animateRemove(View view) {
            setAnimationState(view, AnimateState.HIDE, "animate REMOVE");
            mTargetTranslation = getMeasuredWidth();
            animate(view);
        }

        void animate(View view) {
            if (mScroller == null) {
                mScroller = new Scroller(getContext(), new FastOutSlowInInterpolator());
            } else if (!mScroller.isFinished()) {
                mScroller.forceFinished(false);
            }
            mTargetView = view;
            mScroller.startScroll((int) mTargetView.getTranslationX(), 0, (int) mTargetTranslation, 0);
            mScroller.setFinalX((int) mTargetTranslation);
            mScroller.extendDuration(600);
            ViewCompat.postOnAnimation(view, mAnimationRunnable);
        }

        void setPendingRemove(boolean should) {
            isPendingRemove = should;
        }

        void setExternalRemoved(boolean externalRemoved) {
            isExternalRemoved = externalRemoved;
        }

        private Runnable mAnimationRunnable = new Runnable() {
            @Override
            public void run() {
                if (mScroller.computeScrollOffset()) {
                    mTargetView.setTranslationX(mScroller.getCurrX());
                    invalidate();
                    ViewCompat.postOnAnimation(mTargetView, this);
                } else if (mAnimateState == AnimateState.HIDE) {
                    Log.e(TAG, "finish hide");
                    setAnimationState(mTargetView, AnimateState.NONE, "animate runnable END HIDE");
                    executeRemove(findInfo(mTargetView));
                } else if (mAnimateState == AnimateState.SHOW) {
                    Log.e(TAG, "finish show");
                    setAnimationState(mTargetView, AnimateState.NONE, "animate runnable END SHOW");
                    mPendingAnimateRemove.remove(findInfo(mTargetView).key);
                    if (isPendingRemove && !isCoveredByNext(findInfo(mTargetView))) {
                        destroy(findInfo(mTargetView), "animation end");
                    }
                }
            }
        };

    }

    static class PageInfo {
        Serializable key;
        transient Fragment fragment;

        int drawOrder;
        int childIndex;
        int adapterIndex;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            PageInfo info = (PageInfo) o;

            return key != null ? key.equals(info.key) : info.key == null;

        }

        @Override
        public int hashCode() {
            return key != null ? key.hashCode() : 0;
        }

        @Override
        public String toString() {
            return getClass().getSimpleName() + "[@" + Integer.toHexString(hashCode()) + "]: " + key;
        }
    }

    public static abstract class StackPageAdapter<T extends Serializable, FragmentType extends Fragment> {

        public final String TAG = getClass().getSimpleName();

        private Fragment mCurrentPrimaryItem;
        private FragmentManager mFragmentManager;
        private FragmentTransaction mTransaction;
        private StackAdapterObserver mPagerObserver;

        private final KeyArray<Serializable, T> mItems = new KeyArray<>();

        public StackPageAdapter(FragmentManager manager) {
            mFragmentManager = manager;
        }

        public StackPageAdapter(FragmentManager manager, List<T> items) {
            this(manager);
            addAll(items);
        }

        void setPagerObserver(StackAdapterObserver observer) {
            mPagerObserver = observer;
        }

        void notifyInsert(Serializable key, T item) {
            if (mPagerObserver != null) {
                mPagerObserver.onInsert(key, item);
            }
        }

        void notifyRemove(Serializable key, T item) {
            if (mPagerObserver != null) {
                mPagerObserver.onRemove(key, item);
            }
        }

        Serializable topKey() {
            final int size = mItems.size();
            return size > 0 ? mItems.keyAt(size - 1) : null;
        }

        Serializable previousKey() {
            final int size = mItems.size();
            return size > 1 ? mItems.keyAt(size - 2) : null;
        }

        public ArrayList<T> collectItems() {
            final int size = mItems.size();
            final ArrayList<T> list = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                list.add(mItems.valueAt(i));
            }
            return list;
        }

        public int indexOfKey(Serializable key) {
            return mItems.indexOfKey(key);
        }

        public int indexOfValue(Serializable key) {
            return mItems.indexOfKey(key);
        }

        public void add(T item) {
            final Serializable key = keyFor(item);
            if (!mItems.containsKey(key)) {
                mItems.put(key, item);
                notifyInsert(key, item);
            }
            notifyDataSetChanged();
        }

        public void addAll(List<T> objects) {
            boolean shouldNotify = false;
            for (T item : objects) {
                final Serializable key = keyFor(item);
                if (!mItems.containsKey(item)) {
                    mItems.put(key, item);
                    notifyInsert(key, item);
                    shouldNotify = true;
                }
            }
            if (shouldNotify) {
                notifyDataSetChanged();
            }
        }

        public void swap(List<T> objects) {
            mItems.clear();
            for (T item : objects) {
                final Serializable key = keyFor(item);
                mItems.put(key, item);
            }
            if (mPagerObserver != null) {
                mPagerObserver.onSwap();
            }
            notifyDataSetChanged();
        }

        public void removeByValue(T item) {
            final Serializable key = keyFor(item);
            if (mItems.removeByKey(key) != null) {
                notifyRemove(key, item);
                notifyDataSetChanged();
            }
        }

        public void removeByKey(Serializable key) {
            T item = mItems.removeByKey(key);
            if (item != null) {
                notifyRemove(key, item);
                notifyDataSetChanged();
            }
        }

        public T get(int position) {
            return mItems.valueAt(position);
        }

        public T top() {
            return mItems.size() > 0
                    ? mItems.valueAt(mItems.size() - 1)
                    : null;
        }

        public T previous() {
            return mItems.size() > 1 ?
                    mItems.valueAt(mItems.size() - 2)
                    : null;
        }

        public int getSize() {
            return mItems.size();
        }

        public void beginUpdate() {
            if (mTransaction == null) {
                mTransaction = mFragmentManager.beginTransaction();
            }
        }

        public Fragment instantiateItem(Object key, int containerId) {
            Log.e(TAG, "instantiateItem");
            beginUpdate();
            final String tag = createTag(key, containerId);
            Fragment fragment = this.mFragmentManager.findFragmentByTag(tag);
            if (fragment == null) {
                Log.e(TAG, "create new fragment: " + key);
                fragment = createFragment(mItems.get(key), key);
                mTransaction.add(containerId, fragment, tag);
            } else if (!fragment.isAdded()) {
                Log.e(TAG, "use exists fragment: " + key);
                fragment.setMenuVisibility(false);
                fragment.setUserVisibleHint(false);
            }
            return fragment;
        }

        public void destroyItem(Object key, int containerId) {
            beginUpdate();
            final Fragment fragment = findFragment(key, containerId);
            mTransaction.remove(fragment);
        }

        public void finishUpdate() {
            Log.e(TAG, "finishUpdate: " + mTransaction);
            if (mTransaction != null && !mFragmentManager.isDestroyed()) {
                mTransaction.commitAllowingStateLoss();
                mTransaction = null;
            }
        }

        public void setPrimaryItem(Object key, int containerId) {
            Fragment fragment = findFragment(key, containerId);
            if (fragment != mCurrentPrimaryItem) {
                if (mCurrentPrimaryItem != null) {
                    mCurrentPrimaryItem.setMenuVisibility(false);
                    mCurrentPrimaryItem.setUserVisibleHint(false);
                }
                if (fragment != null) {
                    fragment.setMenuVisibility(true);
                    fragment.setUserVisibleHint(true);
                    onPrimaryItemChange((FragmentType) fragment);
                }
                mCurrentPrimaryItem = fragment;
            }
        }

        public Fragment findFragment(Object key, int containerId) {
            final String tag = createTag(key, containerId);
            return mFragmentManager.findFragmentByTag(tag);
        }

        public void notifyDataSetChanged() {
            if (mPagerObserver != null) {
                mPagerObserver.onChanged();
            }
        }

        public Parcelable saveState() {
            Bundle bundle = new Bundle();
            if (mItems.size() > 0) {
                ArrayList<Serializable> items = new ArrayList<>();
                for (int i = 0; i < mItems.size(); i++) {
                    items.add(mItems.valueAt(i));
                }
                bundle.putSerializable("items", items);
            }
            return bundle;
        }

        public void restoreState(Parcelable state, ClassLoader loader) {
            Bundle bundle = (Bundle) state;
            if (bundle.containsKey("items")) {
                ArrayList<T> items = (ArrayList<T>) bundle.getSerializable("items");
                for (T item : items) {
                    mItems.put(keyFor(item), item);
                }
            }
        }

        public void onPrimaryItemChange(FragmentType fragment) {
        }

        public abstract Serializable keyFor(T item);

        public abstract Fragment createFragment(T item, Object key);


        public static class StackAdapterObserver<T> extends DataSetObserver {
            public void onInsert(Serializable key, T item) {
            }

            public void onRemove(Serializable key, T item) {
            }

            public void onSwap() {
            }
        }

    }

    private static String createTag(Object key, int viewId) {
        return "maestro:switcher:" + viewId + "(" + key + ")";
    }

    public static class SavedState extends AbsSavedState {

        ClassLoader loader;
        Parcelable adapterState;
        ArrayList<Serializable> adapterKeys;

        public SavedState(Parcelable superState) {
            super(superState);
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeParcelable(adapterState, flags);
            out.writeSerializable(adapterKeys);
        }

        @Override
        public String toString() {
            return "StackPager.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + "}";
        }

        public static final Parcelable.Creator<SavedState> CREATOR = ParcelableCompat.newCreator(
                new ParcelableCompatCreatorCallbacks<SavedState>() {
                    @Override
                    public SavedState createFromParcel(Parcel in, ClassLoader loader) {
                        return new SavedState(in, loader);
                    }

                    @Override
                    public SavedState[] newArray(int size) {
                        return new SavedState[size];
                    }
                });

        SavedState(Parcel in, ClassLoader loader) {
            super(in, loader);
            if (loader == null) {
                loader = getClass().getClassLoader();
            }
            adapterState = in.readParcelable(loader);
            adapterKeys = (ArrayList<Serializable>) in.readSerializable();
            this.loader = loader;
        }
    }

}