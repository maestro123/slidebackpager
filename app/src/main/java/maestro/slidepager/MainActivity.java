package maestro.slidepager;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class MainActivity extends AppCompatActivity {

    SlidePager pager;
    TestAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAdapter.add(new AtomicInteger(mAdapter.getCurrent() == null ? 0 : mAdapter.getCurrent().get() + 1));
            }
        });

        findViewById(R.id.remove_current).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAdapter.remove(mAdapter.getCurrent());
            }
        });

        findViewById(R.id.remove_previous).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAdapter.remove(mAdapter.getObject(mAdapter.getItemCount() - 2));
            }
        });

        findViewById(R.id.go_to_middle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int index = mAdapter.getItemCount() >> 1;
                pager.setCurrentPosition(index);
            }
        });

        pager = (SlidePager) findViewById(R.id.pager);
        pager.setAdapter(mAdapter = new TestAdapter(pager, getSupportFragmentManager()));
    }

    private class TestAdapter extends SlidePager.SimpleSlideAdapter<AtomicInteger, Fragment> {

        public TestAdapter(SlidePager pager, FragmentManager fm) {
            super(pager, fm);
        }

        @Override
        public Fragment getItem(AtomicInteger serializable, int position) {
            return TestPage.create(position);
        }

        @Override
        public Object getItemKey(AtomicInteger serializable, int position) {
            return "Fragment #" + serializable.get();
        }

        @Override
        public void saveState(Bundle state) {
            super.saveState(state);
        }

        @Override
        public void restoreState(Bundle bundle) {
            super.restoreState(bundle);
        }
    }

    public static class TestPage extends Fragment {

        public static TestPage create(int position) {
            TestPage page = new TestPage();
            Bundle args = new Bundle(1);
            args.putInt("t", position);
            page.setArguments(args);
            return page;
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            return inflater.inflate(R.layout.test_page_view, container, false);
        }

        @Override
        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);

            TextView textView = (TextView) view.findViewById(R.id.title);
            textView.setText("TEST FRAGMENT: " + (savedInstanceState != null ? savedInstanceState.getInt("sp") + "(saved)" : getArguments().getInt("t")));

            Random random = new Random();
            view.setBackgroundColor(Color.rgb(random.nextInt(255), random.nextInt(255), random.nextInt(255)));

            ArrayList<String> items = new ArrayList<>();
            for (int i = 0; i < 100; i++) {
                items.add("String #" + i);
            }

            ListView listView = (ListView) view.findViewById(R.id.list);
            listView.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, items));

        }

        @Override
        public void onSaveInstanceState(Bundle outState) {
            super.onSaveInstanceState(outState);
            outState.putInt("sp", getArguments().getInt("t"));
        }
    }

}
